package test.inacap.mynuevaapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    private TextView tvtemp;
    private TextView tvtemp1;
    private TextView tvtemp2;
    private TextView tvpress;
    private TextView tvhumid;
    private TextView tvpress1;
    private TextView tvhumid1;
    private TextView tvpress2;
    private TextView tvhumid2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.tvtemp = (TextView) findViewById(R.id.tvtemp);
        this.tvtemp1 = (TextView) findViewById(R.id.tvtemp1);
        this.tvtemp2 = (TextView) findViewById(R.id.tvtemp2);
        this.tvpress = (TextView) findViewById(R.id.tvpress);
        this.tvhumid = (TextView) findViewById(R.id.tvhumid);
        this.tvpress1 = (TextView) findViewById(R.id.tvpress1);
        this.tvhumid1 = (TextView) findViewById(R.id.tvhumid1);
        this.tvpress2 = (TextView) findViewById(R.id.tvpress2);
        this.tvhumid2 = (TextView) findViewById(R.id.tvhumid2);

        String url = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6066399&lon=-72.1034393&pressure=1022.0&humidity=62.0&appid=392bc7318ebcf1c85e62239d1faabeda$units=metric";


        StringRequest solicitud = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), "Solicitando temperatura", Toast.LENGTH_SHORT).show();
                        // Tenemos respuesta desde el servidor
                        try {

                            JSONObject respuestaJSON = new JSONObject(response);
                            String name = respuestaJSON.getString("name");


                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            int temp = tempJSON.getInt("temp");
                            int press = tempJSON.getInt("pressure");
                            int humid = tempJSON.getInt("humidity");



                            Toast.makeText(getApplicationContext(), "Ciudad:" + name + "Temperatura;" + temp, Toast.LENGTH_SHORT).show();
                            tvtemp.setText("Temperatura en Chillán: " + temp);
                            tvpress.setText("La presión en Chillán:"+ press);
                            tvhumid.setText("La humedad en Chillán:" +humid);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                        Log.e("CONEXION", error.getMessage());
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        );

        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitud);


        String url1 = "http://api.openweathermap.org/data/2.5/weather?lat=-33.4726900&lon=-70.6472400&appid=392bc7318ebcf1c85e62239d1faabeda&pressure=1018.0&humidity=31.0$units=metric";

        StringRequest solicitud1 = new StringRequest(
                Request.Method.GET,
                url1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), "Solicitando temperatura", Toast.LENGTH_SHORT).show();
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            String name1 = respuestaJSON.getString("name");


                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            int temp1 = tempJSON.getInt("temp");
                            int press1 = tempJSON.getInt("pressure");
                            int humid1 = tempJSON.getInt("humidity");

                            Toast.makeText(getApplicationContext(), "Ciudad:" + name1 + "Temperatura;" + temp1, Toast.LENGTH_SHORT).show();
                            tvtemp1.setText("Temperatura en Santiago: " + temp1);
                            tvpress1.setText("La presión en Santiago: " + press1);
                            tvhumid1.setText("La humedad en Santiago: " + humid1);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                        Log.e("CONEXION", error.getMessage());
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        );

        RequestQueue listaEspera1 = Volley.newRequestQueue(getApplicationContext());
        listaEspera1.add(solicitud1);


        String url2 = "http://api.openweathermap.org/data/2.5/weather?lat=-36.8282&lon=-73.0514&appid=392bc7318ebcf1c85e62239d1faabeda&pressure=1023.0&humidity=71.0$units=metric";

        StringRequest solicitud2 = new StringRequest(
                Request.Method.GET,
                url2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), "Solicitando temperatura", Toast.LENGTH_SHORT).show();
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            String name2 = respuestaJSON.getString("name");


                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            int temp2 = tempJSON.getInt("temp");
                            int press2 = tempJSON.getInt("pressure");
                            int humid2 = tempJSON.getInt("humidity");

                            Toast.makeText(getApplicationContext(), "Ciudad:" + name2 + "Temperatura;" + temp2, Toast.LENGTH_SHORT).show();
                            tvtemp2.setText("Temperatura en Concepción: " + temp2);
                            tvpress2.setText("La presión en Concepción: " + press2);
                            tvhumid2.setText("La humedad en Concepción: " + humid2);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                        Log.e("CONEXION", error.getMessage());
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        );

        RequestQueue listaEspera2 = Volley.newRequestQueue(getApplicationContext());
        listaEspera2.add(solicitud2);
    }
}



